<?php

namespace App;

use App\User;
use App\Discussion;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
		'body', 'user_id', 'discussion_id'
	];
	/**
	 * discussion relationship
	 * @return boolean
	 */
    public function discussion()
    {
    	return $this->belongsTo(Discussion::class);
    }

    /**
     * user relationship
     * @return boolean 
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
