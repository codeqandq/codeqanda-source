<?php

namespace App;

use App\User;
use App\Channel;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $fillable = [
    	'title', 'body', 'slug', 'user_id', 'channel_id', 'channel', 'updatedUserName'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function channel()
    {
    	return $this->belongsTo(Channel::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
