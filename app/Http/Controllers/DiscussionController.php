<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Comment;
use App\Discussion;
use App\Discussion_User;
use App\Http\Requests;
use App\Mail\DiscussionUpdatedEmail;
use App\Mail\WelcomeEmail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use League\CommonMark\CommonMarkConverter;

class DiscussionController extends Controller
{
	/**
     * Discussion View with Channels
	 * @return boolean
	 */
    public function create()
    {
    	$channels = Channel::orderBy('channel', 'asc')->get();

    	return view('discussions.create')->withChannels($channels);
    }


    /**
     * Create Discussion
     * @param  Request $request 
     * @return boolean
     */
    public function store(Request $request)
    {
    	$request['slug'] = str_slug($request->title);
    	$channel = Channel::where('channel', $request->channel)->first();
    	$request['channel_id'] = $channel->id;
    	$request['user_id'] = Auth::user()->id;
        $request['updatedUserName'] = Auth::user()->userName;

    	$discussion = Discussion::create($request->all());

        // Add to to discussion_user table if user selected to be notified.
        if ( $request->emailNotify != null ) {
            Auth::user()->discussionUpdates()->toggle($discussion);
        }

    	return redirect('/discuss');
    }

    /**
     * Show all discussions on /discuss page
     * @return boolean
     */
    public function index()
    {
    	$discussions = Discussion::orderBy('updated_at', 'desc')->paginate(15);
    	$channels = Channel::orderBy('channel', 'asc')->get();

    	return view('discussions.index')->withChannels($channels)
    									->withDiscussions($discussions);
    }

    /**
     * show specific discussion
     * @param  $channel   
     * @param  Discussion $discussion 
     * @param  $discussionSlug
     * @return boolean
     */
    public function show($channel, Discussion $discussion, $discussionSlug)
    {
    	$comments = Comment::where('discussion_id', $discussion->id)->get();
        $converter = new CommonMarkConverter();

        return view('discussions.show', compact('channel', 'discussion', 'discussionSlug', 'comments', 'converter'));
    }

    /**
     * Add Comment to Discussion
     * @param  Discussion $discussion 
     * @param  Request    $request   
     * @return boolean
     */
    public function comment(Discussion $discussion, Request $request)
    {
        $userWantsToBeNotified = Discussion_User::where('discussion_id', $discussion->id)
                                                ->where('user_id', Auth::user()->id)
                                                ->get();
        $emailList = Discussion_User::where('discussion_id', $discussion->id)->get();
        $userWhoUpdated = Auth::user()->userName;

    	$request['discussion_id'] = $discussion->id;
    	$request['user_id'] = Auth::user()->id;

        DB::table('discussions')
                    ->where('id', $discussion->id)
                    ->update(['updated_at' => Carbon::now()]);

        DB::table('discussions')
                    ->where('id', $discussion->id)
                    ->update(['updatedUserName' => Auth::user()->userName]);

    	$comment = Comment::create($request->all());

        foreach ($emailList as $email) {
            $userEmailAddress = Auth::user()->where( 'id', $email->user_id )->select('email')->get();
            $userName = User::where( 'id', $email->user_id )->pluck('name');

            Mail::to($userEmailAddress)->send(new DiscussionUpdatedEmail($discussion, $userName[0], $comment->body, $userWhoUpdated));
        }
        // Add to to discussion_user table if user selected to be notified.
        if ( $request->emailNotify != null ) {
            if ( count($userWantsToBeNotified) < 1) {
                Auth::user()->discussionUpdates()->toggle($discussion);
            } 
        }

    	return back();
    }

    /**
     * User Liked Comment
     * @param  Comment $comment 
     * @return boolean
     */
    public function like(Comment $comment)
    {
        if ( ! ($this->checkCommentLikeForUserName($comment->id)) ) {
            Redis::rpush('commentLike' . $comment->id, Auth::user()->userName);
        }

        return back();

    }

    /**
     * Show all Discussions for Channel
     * @param  $channel 
     * @return boolean
     */
    public function channel($channel)
    {
        $discussions = Discussion::where('channel', $channel)->orderBy('created_at', 'desc')->paginate(15);
        $channels = Channel::orderBy('channel', 'asc')->get();

        return view('discussions.index')->withChannels($channels)
                                        ->withDiscussions($discussions);
    }

    /**
     * See if User Has Already Liked Comment
     * @param  $comment_id 
     * @return boolean
     */
    public function checkCommentLikeForUserName($comment_id)
    {
        $list = Redis::lrange('commentLike' . $comment_id, 0, -1);

        foreach ($list as $user) {
            if ($user == Auth::user()->userName) {
                return true;
            } 
        }

        return false;
    }    

    /**
     * Show Users Profile with Recent Data Available to View.
     * @param  $userName
     * @return boolean
     */
    public function userProfile($userName)
    {
        $user = User::where('userName' , $userName)->firstOrFail();
        $userDiscussions = Discussion::where('user_id', $user->id)->orderBy('created_at', 'desc')->take(5)->get();
        $userComments = Comment::where('user_id', $user->id)->orderBy('created_at', 'desc')->take(5)->get();

        return view('userprofile')->withUser($user)
                                  ->withDiscussions($userDiscussions)
                                  ->withComments($userComments);
    }      

    /**
     * Ajax Request from Show view to best_answer the comment
     * @param  Discussion $discussion 
     * @param  Comment    $comment    
     * @param  User       $user   
     * @return boolean
     */
    public function bestAnswer(Discussion $discussion, Comment $comment, User $user)
    {
        Discussion::where('id', $discussion->id)->update(['best_answer' => $comment->id]);
    }
}

















