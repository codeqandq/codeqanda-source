<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Auth::user()->email;
        // $email = 'billy@codeqanda.com';
        $default = "http://www.diaglobal.org/_Images/member/Generic_Image_Missing-Profile.jpg";
        $size = 200;
        
        $gravUrl = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

        return view('home', compact('gravUrl'));
    }

    public function update(User $user, Request $request)
    {
        $user->update($request->all());

        flash()->overlay('Your Information Has Been Updated!', 'Updated!');

        return back();
    }

    public function userProfile($userName)
    {
        $user = User::where('userName' , $userName)->firstOrFail();

        return view('userprofile')->withUser($user);
    }

    


}
