<?php

namespace App\Mail;

use App\Discussion;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DiscussionUpdatedEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $discussion;
    public $userName;
    public $comment;
    public $userWhoUpdated;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Discussion $discussion, $userName, $comment, $userWhoUpdated)
    {
        $this->discussion = $discussion;
        $this->userName = $userName;
        $this->comment = $comment;
        $this->userWhoUpdated = $userWhoUpdated;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@codeqanda.com')
                    ->subject($this->discussion->title . ' Has Been Updated @ CodeQandA.com')
                    ->view('emails.discussionUpdated');
    }
}
