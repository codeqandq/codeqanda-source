<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder 
{
	public function run()
	{
		App\Channel::create(['channel' => 'CodeReview', 'color' => 'green']);
		App\Channel::create(['channel' => 'Elixir', 'color' => 'gold']);
		App\Channel::create(['channel' => 'Eloquent', 'color' => 'turquoise']);
		App\Channel::create(['channel' => 'Envoyer', 'color' => 'tomato']);
		App\Channel::create(['channel' => 'Forge', 'color' => 'steelblue']);
		App\Channel::create(['channel' => 'General', 'color' => 'blue']);
		App\Channel::create(['channel' => 'Guides', 'color' => 'maroon']);
		App\Channel::create(['channel' => 'Laravel', 'color' => 'orangered']);
		App\Channel::create(['channel' => 'Lumen', 'color' => 'lightpink']);
		App\Channel::create(['channel' => 'Requests', 'color' => 'chocolate']);
		App\Channel::create(['channel' => 'Servers', 'color' => 'greenyellow']);
		App\Channel::create(['channel' => 'SiteFeedback', 'color' => 'springgreen']);
		App\Channel::create(['channel' => 'Spark', 'color' => 'darkslateblue']);
		App\Channel::create(['channel' => 'Testing', 'color' => 'firebrick']);
		App\Channel::create(['channel' => 'Tips', 'color' => 'forestgreen']);
		App\Channel::create(['channel' => 'Vue', 'color' => 'seagreen']);
		App\Channel::create(['channel' => 'PHP', 'color' => 'skyblue']);
		App\Channel::create(['channel' => 'Javscript', 'color' => 'lightsalmon']);
	}
}