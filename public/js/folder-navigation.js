(function () {
	var url = window.location.pathname;
	var urlPieces = url.split('/');
	var navigationLength;

	switch (urlPieces.length) {
		case 2: 
			document.write('<a href="/discuss"><span class="folder-up-style">Discuss</span></a>');
			break;
		case 3:
			document.write('<a href="/discuss"><span class="folder-up-style">Discuss</span></a> / <span class="folder-active-style">' + sentenceCase(urlPieces[2]) + '</span>');
			break;
		case 4:
			navigationLength = 3;
			break;
		case 5:
			navigationLength = 4;
			break;
		case 6:
			document.write('<a href="/discuss"><span class="folder-up-style">Discuss</span></a> / ' +
				'<a href="/discuss/' + urlPieces[3] + '"><span class="folder-up-style">' + 
				sentenceCase(urlPieces[3]) + '</span></a> / <span class="folder-active-style">' +
				sentenceCase(urlPieces[5].replace('-', ' ')) + '</span>');
			break;
		case 7:
			navigationLength = 6;
			break;
		default: 
			navigationLength = false;
	}

	function sentenceCase (str) {  
	  if ((str===null) || (str===''))  
	       return false;  
	  else  
	   str = str.toString();  
	  
	 return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});  
	}  

})();