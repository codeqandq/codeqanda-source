$(document).ready(function() {
  $.uploadPreview({
    input_field: "#profilePicture",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#profilePictureLabel",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});