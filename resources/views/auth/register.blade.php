@extends('layouts.template')

@section('stylesheets')
    <link rel="stylesheet" href="/css/register.css">
    <link rel="stylesheet" href="http://js.nicdn.de/bootstrap/formhelpers/docs/assets/css/bootstrap-formhelpers.css">
    <link rel="stylesheet" href="http://js.nicdn.de/bootstrap/formhelpers/docs/assets/css/bootstrap-formhelpers-countries.flags.css">
    <link rel="stylesheet" href="http://js.nicdn.de/bootstrap/formhelpers/docs/assets/css/bootstrap-formhelpers-currencies.flags.css">
    <link rel="stylesheet" href="/css/flags.css">
@stop

@section('pageTitle')
  Web Development Forum CodeQandA.com Registration Page
@stop

@section('scriptsHeader')
     <script type="text/javascript" src="/js/jquery.uploadPreview.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/bootstrap-formhelpers.min.js"></script>
    <script src="/js/countries.js"></script>
    <script src="/js/countries.en.js"></script>
@stop



@section('content')
<script type="text/javascript">
$(document).ready(function() {
  $.uploadPreview({
    input_field: "#profilePicture",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#profilePictureLabel",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false                 // Default: false
  });
});
</script>


    <div class="register-nav-div">
        <a href="/discuss" class="btn btn-primary btn-lg register-nav-btn-discussion">Discussion</a>
    </div>


    <div class="register-wrapper">
        <div class="register-heading">
            <span class="header-text">Register Your Credentials</span>
        </div>



        <div class="register-form">

        <span class="small-red-text">*  Required</span>

        {!! Form::open(['method' => 'POST', 'url' => '/register', 'files' => 'true', 'class' => 'form-horizontal']) !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', 'Name', ['class' => '']) !!}<span class="small-red-text"> *</span>
                {!! Form::text('name', null, ['class' => 'form-control ', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>

            <div class="form-group">
              <label for="country" value="Country">Country</label></br>
              <select class="input-medium bfh-countries" data-country="US" name="country"></select>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email Address', ['class' => '']) !!}<span class="small-red-text"> *</span>
                {!! Form::text('email', null, ['class' => 'form-control ', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>

            <div class="form-group{{ $errors->has('userName') ? ' has-error' : '' }}">
                {!! Form::label('userName', 'Username', ['class' => '']) !!}<span class="small-red-text"> *</span>
                {!! Form::text('userName', null, ['class' => 'form-control ', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('userName') }}</small>
            </div>

          </br><span class="small-red-text">Profile Pictures are pulled from <a href="http://gravatar.com">Gravatar</a> - Sign up Now</span></br></br>

           <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
               {!! Form::label('password', 'Password', ['class' => '']) !!}<span class="small-red-text"> *</span>
               {!! Form::password('password', ['class' => 'form-control ', 'required' => 'required']) !!}
               <small class="text-danger">{{ $errors->first('password') }}</small>
           </div>

           <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
               {!! Form::label('password_confirmation', 'Confirm Password', ['class' => '']) !!}<span class="small-red-text"> *</span>
               {!! Form::password('password_confirmation', ['class' => 'form-control ', 'required' => 'required']) !!}
               <small class="text-danger">{{ $errors->first('confirm_password') }}</small>
           </div>

            <div class="btn-group pull-right">
                {!! Form::submit("Register", ['class' => 'btn btn-primary']) !!}
            </div>


        
        {!! Form::close() !!}
        </div>
        </div>







@endsection

@section('scriptsBottom')
    <script type="text/javascript" src="/js/jquery.uploadPreview.min.js"></script>
@stop
