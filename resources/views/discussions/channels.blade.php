<style>

.view-channel {
	float: left;
	position: relative;
	width: 200px;
}

.view-channel-bar-and-channel {
	margin-bottom: .7rem;
}

.view-channel-bar {
	position: absolute;
	height: 1rem;
	width: 90%;
	float: left;
	clear: both;
	background-color: red;
	margin-top: 3px;
	opacity: .4;
	border-radius: 10px;
	margin-bottom: 3rem;
}

.view-channel-channel {
	font-size: 1.5rem;
	position: absolute;
	color: #cccccc;
	clear: both;
	float: left;
	background-color: rgb(245, 245, 241);
}
</style>

<div class="view-channel">
	@foreach ($channels as $channel)
	<a href="/discuss/{{ $channel->channel }}">
	<div class="view-channel-bar-and-channel">
		<div class="view-channel-bar" 
		     style="background-color: {{ $channel->color }};"
		     id="channelbar{{ $channel->color }}">
		</div>
		<div class="view-channel-channel"
		     id="channel{{ $channel->color }}">{{ $channel->channel }}&nbsp&nbsp</div>
		
	</div></br>
	</a>
		<script>

			$('#channelbar{{ $channel->color }}').mouseenter( function() {
				$('#channelbar{{ $channel->color }}').animate({ "width": "-=20px" }, 100);
				$('#channelbar{{ $channel->color }}').css('opacity', '1');
			});

			$('#channelbar{{ $channel->color }}').mouseleave( function() {
				$('#channelbar{{ $channel->color }}').animate({ "width": "+=20px" }, 100);
				$('#channelbar{{ $channel->color }}').css('opacity', '.6');
			});
		</script>

	@endforeach	
</div>


<script>

</script>











