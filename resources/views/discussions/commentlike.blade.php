{!! $userHasLiked = false !!}
<?php $commentLikes = Redis::lrange('commentLike' . $comment->id, 0, -1); ?>
@foreach ($commentLikes as $like)
@if (Auth::user())
	@if ( $like == Auth::user()->userName )
		<?php $userHasLiked = true ?>
	@else 
		<?php  $userHasLiked = false ?>
	@endif
@else 
	<?php $userHasLiked = false ?>
@endif
@endforeach

<style>

	.show-comment-likes a {
		color: orange !important;
		text-decoration: none;
	}

	.show-comment-likes a:visited {
		color: orange !important;
		text-decoration: none;
	}

	.show-comment-likes a:hover {
		color: orange !important;
		text-decoration: none;
	}

</style>




<div class="show-comment-likes">
	@if ($userHasLiked != true && Auth::user() )
			<a href="/comment/like/{{ $comment->id }}"><span class="glyphicon glyphicon-thumbs-up show-thumbs-up" aria-hidden="true"></span></a></br>
		@foreach ($commentLikes as $commentLike)
	 		<a href="/users/{{ $commentLike }}"><span class="show-comment-like">{{ $commentLike }}</span></a>
	 	@endforeach
	@else
			@if (count($commentLikes) < 1)
				{{-- nothing to do if there are no comments and the user is not logged in. --}}
			@else
				<span class="show-comment-like-by">Liked By</span></br>
			 	@foreach ($commentLikes as $commentLike)
			 		<a href="/users/{{ '@' . $commentLike }}"><span class="show-comment-like">{{ $commentLike }}</span></a>
			 	@endforeach
		 	@endif
	@endif
	</div>




                