@extends('layouts.template')

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="/css/create.css">
@stop

@section('scriptsHeader')
	<script src="https://unpkg.com/vue@2.0.3/dist/vue.js"></script>
	<script src="/js/main.js"></script>
@stop

@section('content')



	@include('discussions.channels', $channels)

<div id="app">
<div class="create-wrapper">
<div class="create-form">
	{!! Form::open(['method' => 'POST', 'url' => 'discuss/create', 'class' => 'form-horizontal']) !!}
	
		<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		    {!! Form::text('title', null, ['class' => 'form-control create-title', 'required' => 'required', 'placeholder' => 'Type Here to Choose a Title', 'maxlength' => '150', 'v-model' => 'vueDiscussionTitle']) !!}
		    <small class="text-danger">{{ $errors->first('title') }}</small>
		</div>

		<div class="create-channel-bar">
			<div class="create-channel-select">
			Channel&nbsp&nbsp
				<select name="channel">
					@foreach ($channels as $channel)
						<option value="{{ $channel->channel }}">{{ $channel->channel }}</option>
					@endforeach
				</select>
		
				
			
			</div>
		</div>

		<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
		    {!! Form::textarea('body', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'What is your question?']) !!}
		    <small class="text-danger">{{ $errors->first('body') }}</small>
		</div>




	</div>

	</div>

		<div class="checkbox create-checkbox-email">
		  <label><input type="checkbox" value="email" name="emailNotify" checked>Receive Email Notifications For This Discussion</label>
		</div>

		<div class="create-submit">
	        {!! Form::submit("Post Your Question", ['class' => 'btn create-submit']) !!}
	    </div>
	{!! Form::close() !!}


</div>


@stop

