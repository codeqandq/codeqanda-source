@extends('layouts.template')

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="/css/discuss.css">
	<link rel="stylesheet" href="/css/flags.css">
@stop

@section('pageTitle')
	Web Development Forum Discussion Page - Discuss anything about Web Development
@stop

@section('scriptsHeader')
	<script src="/js/bootstrap-formhelpers.min.js"></script>
@stop
@section('content')
	@include('discussions.channels', $channels)

	<div class="discuss-wrapper">
		@foreach ($discussions as $discussion)
			<div class="discuss-individual-wrapper">
			<div>
				<div class="discuss-individual-first-div">
					<div class="first-text">
					@if ($discussion->best_answer)
						<img src="/img/check.png" class="check-mark">
					@endif
						<img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( $discussion->user->email ) ) )}}?d={{ urlencode( "http://www.diaglobal.org/_Images/member/Generic_Image_Missing-Profile.jpg" )}}&s={{ 40 }}"
							 class="discuss-individual-profile">
							 {{-- <span class="bfh-countries" data-country="{{ $discussion->user->country }}" data-flags="true"></span> --}}
					</div>
				</div>
				<div class="discuss-individual-second-div">
					<div class="discuss-individual-title">
						<a href="/discuss/channels/{{ strtolower($discussion->channel) }}/{{ $discussion->id }}/{{ $discussion->slug }}">{{ $discussion->title }}</a>
					</div>
					<div class="discuss-individual-updated-at">
						Updated <a href="/discuss/channels/{{ strtolower($discussion->channel) }}/{{ $discussion->id }}/{{ $discussion->slug }}">{{ $discussion->updated_at->diffForHumans() }}</a> by <a href="/users/{{ '@' . $discussion->user->userName }}">{{ $discussion->updatedUserName }}</a>.
					</div>
				</div>
				<div class="discuss-individual-fourth-div">
					<div class="fourth-text">
						{{ count($discussion->comments )}}
					</div>
				</div>
				<div class="discuss-individual-third-div">
					<div class="third-text">
					 	<div class="discuss-box" style="background-color: {{ App\Channel::where('id', $discussion->channel_id )->pluck('color')->first() }}">&nbsp</div> <a href="/discuss/{{ $discussion->channel }}">{{ $discussion->channel }}</a>
				 	</div>
				</div>
				
			</div>

			</div>

		@endforeach

	</div>

	<div class="pagination">
			{{ $discussions->links() }}
		</div>



@stop    

