@extends('layouts.template')

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="/css/show.css">
	<link rel="stylesheet" href="/css/flags.css">
@stop

@section('pageTitle')
	{{ $discussion->title }}
@stop

@section('scriptsHeader')
	<script src="/js/app.js"></script>
	<script src="/js/bootstrap-formhelpers.min.js"></script>
@stop

@section('content')



	
<div class="show-wrapper">
	<div class="show-discussion-header">
			{{ $discussion->title }}
	</div>
	<div class="show-discussion-profile">
		<img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( $discussion->user->email ) ) )}}?d={{ urlencode( "http://www.diaglobal.org/_Images/member/Generic_Image_Missing-Profile.jpg" )}}&s={{ 120 }}"
								 class="discuss-individual-profile">
	</div>
	<div class="show-discussion-content">
		<div class="show-discussion-by">
			<div class="show-comment-by-flag">
				<span class="bfh-countries" data-country="{{ $discussion->user->country }}" data-flags="true"></span>
			</div>
			<a href="/users/{{ '@' . $discussion->user->userName }}">{{ $discussion->user->userName }}</a> - {{ $discussion->created_at->diffForHumans() }}
		</div>
		<div class="show-discussion-body">
			{!! $converter->convertToHtml($discussion->body) !!}
		</div>

		@if ($discussion->best_answer)
		
			<div class="show-best-answer-content">
				<div class="show-best-answer-header">
				<span class="show-best-answer-heading">Thread Owner Best Answer - </span>
				{{ $comments->where('id', $discussion->best_answer)->pluck('created_at')[0]->diffForHumans() }}
				<span class="glyphicon glyphicon-star show-best-answer-icon" aria-hidden="true"></span>
				<br>
				</div>
				<div class="show-best-answer-body">
				{!! $converter->convertToHtml($comments->where('id', $discussion->best_answer)->pluck('body')[0]) !!}
				</div>
			</div>
		@endif
	</div>
</div>






@foreach ($comments as $comment)
	<div class="show-comment-wrapper">
		<div class="show-comment-profile">


		@if ($discussion->best_answer == $comment->id)
			<img src="/img/check.png" class="show-best-answer">
		@endif
		



			<img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( $comment->user->email ) ) )}}?d={{ urlencode( "http://www.diaglobal.org/_Images/member/Generic_Image_Missing-Profile.jpg" )}}&s={{ 40 }}"
									 class="discuss-individual-profile">
			
		</div>
		<div class="show-comment-content">
			<div class="show-comment-by">
				<div class="show-comment-by-flag">
					<span class="bfh-countries" data-country="{{ $comment->user->country }}" data-flags="true"></span>
				</div>
				<a href="/users/{{ '@' . $comment->user->userName }}" style="color: orange;">{{ $comment->user->userName }}</a> - {{ $comment->created_at->diffForHumans() }}
			</div>
			<div class="show-comment-body">
			
				{!! $converter->convertToHtml($comment->body) !!}
			
			</div>
		</div>
		
		
		@include('discussions.commentlike')


		@if (Auth::user())
		@if (!$discussion->best_answer)
		{{-- <form method="POST" action="/bestanswer/{{ $discussion->id }}/{{ $comment->id }}/{{ $comment->user->id }}"> --}}
		@if (Auth::user()->id == $discussion->user->id)
				<div class="show-answer-checkmark">
					<img src="/img/check.png" class="show-answer-checkmark-img" id="bestAnswer{{ $comment->id }}">
				</div>

			<script>
				var checkMark = $('#bestAnswer{{ $comment->id }}');


				checkMark.on('click', function () {
								$.ajax({
									method: 'get',
									url: '/bestanswer/{{ $discussion->id }}/{{ $comment->id }}/{{ $comment->user->id }}',
									success: function(data) {
										location.reload(true);
									}
								});
							});
							
			</script>
		@endif
			{{-- </form> --}}
		@endif
		@endif
		
		
	</div>
@endforeach






@if (Auth::user())
	<div class="show-comment-wrapper">
		Use GitHub Flavored Markdown Below
	
	<form method="POST" action="/{{ $discussion->id }}/comment" class="form-horizontal">
	{{ csrf_field() }}
		<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
		    {!! Form::textarea('body', null, ['class' => 'form-control', 'id' => 'commentTextArea', 'required' => 'required', 'placeholder' => 'What you got to say?', 'onkeydown' => '']) !!}
		    <small class="text-danger">{{ $errors->first('body') }}</small>
		</div>
<div class="form-group checkbox">
		  <label><input type="checkbox" value="email" name="emailNotify" checked>Receive Email Notifications for Discussion</label>
	</div>

	</div>
	

	        {!! Form::submit("Post Comment", ['class' => 'btn btn-primary show-comment-btn']) !!}
		</form>
@endif


{{-- Disable Tab Default Functionality --}}
<script>
$.fn.getTab = function () {
    this.keydown(function (e) {
        if (e.keyCode === 9) {
            var val = this.value,
                start = this.selectionStart,
                end = this.selectionEnd;
            this.value = val.substring(0, start) + '\t' + val.substring(end);
            this.selectionStart = this.selectionEnd = start + 1;
            return false;
        }
        return true;
    });
    return this;
};

$("textarea").getTab();
</script>
{{-- End disable tab --}}

@stop



