<style>

.userimage-wrapper {
	position: relative;
	width: 50px !important;
	height: 50px !important;
}

.userimage-image {
	float: left;
	width: {{ $userImageWidth }}rem !important;
	border-radius: 50%;
	position: relative;
}

.userimage-flag {
	float: left;
	font-size: 0;
	position: relative;
}

</style>


<div class="userimage-wrapper">

	<div class="userimage-image">
		<img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( $user->email ) ) )}}?d={{ urlencode( "/img/general-profile.jpg" )}}&s={{ 200 }}"
								 class="discuss-individual-profile userimage-image">
	</div>
	<div class="userimage-flag">
		<span class="bfh-countries" data-country="{{ $user->country }}" data-flags="true"></span>
	</div>
	
</div>