@extends('layouts.template')

@section('stylesheets')
	<link rel="stylesheet" href="/css/userprofile.css">
@stop
@section('content')


<div class="profile-wrapper">

	<img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( $user->email ) ) )}}?d={{ urlencode( "/img/general-profile.jpg" )}}&s={{ 200 }}"
								 class="profile-img">


	<div class="profile-info">

		<div class="profile-headings">
			<div class="profile-heading">
				Name: 
			</div>
			<div class="profile-heading">
				Username: 
			</div>
			<div class="profile-heading">
				Email: 
			</div>
			<div class="profile-heading">
				City: 
			</div>
			<div class="profile-heading">
				State: 
			</div>

		</div>
		<div class="profile-heading-info">
			<div class="profile-information">
				{{ $user->name }}
			</div>
			<div class="profile-information">
				{{ $user->userName }}
			</div>
			<div class="profile-information">
				{{ $user->email }}
			</div>
			<div class="profile-information">
				{{ $user->city }}
			</div>
			<div class="profile-information">
				{{ $user->state }}
			</div>
		</div>
	</div>

</div>

<div class="bottom-bar">Recent Activity</div>

@stop