<!DOCTYPE html>
<html>
<head>
	<style>
	body {
		font-size: 18px;
		color: #000;
	}


	.main-logo {
    font-size: 4rem;
    /*font-family: 'PT Mono', monospace;*/
    color: #000000;
    float: left;
    padding-top: 1rem;
    margin-left: 2.5rem;
}

.main-logo-com {
    font-size: 2rem;
    background: rgb(104, 152, 193);
    color: #fff;
    text-align: center;
    width: 900px;
    font-size: 4rem;
    /*font-family: 'PT Mono', monospace;*/
    color: #000000;
    float: left;
    padding-top: 1rem;
    margin-left: 2.5rem;
}

.codeqanda-main-logo {
    width: 5rem;
    transform: rotate(80deg);
}

.email-body {
	text-align: left;
	float: left;
	clear: both;
	border: solid 1px black;
	padding: 2rem;
	margin-left: 100px;
	width: 700px;
}
</style>
</head>
<body>
<div class="main-logo-com">
    Code<img src="{{ $message->embed('img/codeqandalogo.png') }}" class="codeqanda-main-logo" style="transform: rotate(280deg);">andA.com</span>
</div><br><br>
<div class="email-body">
<h3>Hello, {{ $userName }}</h3>

<h3>Discussion {{ $discussion->title }} has been updated by {{ $userWhoUpdated }} (see below)</h3>

<p>
	{{ $comment }}
</p>
<p>
	Visit <a href="http://codeqanda.com/discuss/channels/{{ $discussion->channel }}/{{ $discussion->id }}/{{ $discussion->slug }}">CodeQandA</a> to see the discussion.
</p>
</div>
</body>
</html>
