@extends('layouts.template')

@section('stylesheets')
	<link rel="stylesheet" href="/css/home.css">
    <link rel="stylesheet" href="/css/flags.css">
@stop

@section('scriptsHeader')
    <script src="/js/app.js"></script>
    <script src="/js/bootstrap-formhelpers.min.js"></script>
 
@stop

@section('content')

</style>
<div class="home-link-wrapper">
	<a href="/discuss" class="btn btn-primary home-nav-btn-discussion">Discussion</a>
    <a href="/discuss/create" class="btn btn-primary btn-lg home-nav-btn-new">New Question</a>
</div>
<div class="home-wrapper">
<form method="POST" action="/{{ Auth::user()->id }}/update">
{{ csrf_field() }}
	<div class="form-group">
	    <div class="home-heading">
		    <div class="home-user-name">
		        {{ Auth::user()->userName }}
		    </div>

		    <div class="home-member-since">
		        Member Since {{ Auth::user()->created_at->diffForHumans() }}
		    </div>
	    </div>

	    <div class="home-image-div">
	    
	    	<img src="{{ $gravUrl }}" class="home-profile-image">
	    </div>

	    <div class="home-information-div">

	    <div class="home-form">
	    	<label for="name" class="home-label">Name</label>
		    <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
		</div>

		 <div class="home-form">
	    	<label for="email" class="home-label">Email</label>
		    <input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}">
		</div>

		<div class="home-form">
	    	<label for="city" class="home-label">City</label>
		    <input type="text" class="form-control" name="city" value="{{ Auth::user()->city }}">
		</div>

		 <div class="home-form">
	    	<label for="state" class="home-label">State</label>
		    <input type="text" class="form-control" name="state" value="{{ Auth::user()->state }}">
		</div>

		<div class="home-form home-country">
			<span class="bfh-countries" data-country="{{ Auth::user()->country }}" data-flags="true"></span>
		</div>

		<button type="submit" name="submit" class="btn home-update-btn">Update Information</button>
		</div>
	</div>
</form>

</div>
</div>
@include('flash::message')

<script>
    $('#flash-overlay-modal').modal();
</script>



@endsection