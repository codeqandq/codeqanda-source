<!-- extends('layouts.template')


    

@section('content')
    Landing Page    
@stop
 -->

 <!DOCTYPE html>
 <html>
 <head>
 	<link rel="stylesheet" type="text/css" href="/css/landing.css">	
 	<title>
		Web Development Forum PHP/Laravel/Javascript etc - Code QandA.com
 	</title>
    <meta name="google-site-verification" content="AgfwUgqyGpo-QVB03VjPJR39HudJ2zTH_0JA2bk2x2c" />
 </head>
 <body>
 
 	<div class="top-bar"></div>

 	<div class="main-logo">
                Code <img src="/img/codeqandalogo.png" class="codeqanda-main-logo">and A<span class="main-logo-com">.com</span>

            </div>

 	<div class="middle-section">
 		<div class="slogan-div">
	 		<h1 class="landing-h1">You guessed it.  Ask and you shall recieve.</h1>
	 		<h3>
	 			<p>A great community of developers who are happy to answer and help with any questions you might have regarding web development.</p>
	 			<p>We are a fairly new site, so please make sure to link back to us and help get this site off the ground.</p>
	 		</h3>
 		</div>
 		<img src="/img/landing-logo.png" class="landing-logo">
 	</div>


 	<div class="landing-navigation">
 		<a href="/discuss" class="nav-btn">Discuss</a>
 		<a href="/register" class="nav-btn">Register</a>
 		<a href="/login" class="nav-btn">Login</a>
 	</div>

 </body>
 </html>
