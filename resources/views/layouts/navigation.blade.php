@if (Auth::user())

{{-- Nav if logged in --}}
    <nav class="navbar pull-right">
        <div class="nav-btn-div">
            <a href="/discuss" class="btn nav-btn"><span class="glyphicon glyphicon-comment nav-icon" aria-hidden="true"></span> Discussion</a>
        </div>
        <div class="nav-btn-div">
            <a href="/discuss/create" class="btn nav-btn"><span class="glyphicon glyphicon-pencil nav-icon" aria-hidden="true"></span> New Discussion</a>
        </div>
        <div class="nav-btn-div">
            <a href="/home" class="btn nav-btn">
                <span class="glyphicon glyphicon-home nav-home" aria-hidden="true"></span>
                &nbsp&nbsp&nbsp&nbspHome
            </a>
        </div>
        <div class="nav-btn-div">
            <a href="/logout" class="btn nav-btn nav-btn-right">
                <span class="glyphicon glyphicon-off nav-icon" aria-hidden="true"></span>
                Logout
            </a>
        </div>
    </nav>

@else

{{-- Nav if not logged in --}}
    <nav class="navbar site-nav">
        <div class="nav-btn-div">
            <a href="/discuss" class="btn nav-btn">
                <span class="glyphicon glyphicon-comment nav-icon" aria-hidden="true"></span>
                Discussion
            </a>
        </div>
        <div class="nav-btn-div">
                <a href="/register" class="btn nav-btn">
                    <span class="glyphicon glyphicon-edit nav-icon" aria-hidden="true"></span>
                    Register
                </a>
        </div>
        <div class="nav-btn-div">
                <a href="/login" class="btn nav-btn  nav-btn-right" id="login">
                    <span class="glyphicon glyphicon-off nav-icon" aria-hidden="true"></span>
                    Login
                </a>
        </div>
    </nav>

@endif
