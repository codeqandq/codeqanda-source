<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            @yield('pageTitle')
        </title>

        <meta name="description" content="A web development forum where you can ask questions and receive answers in a very detailed fasion.  Anything from PHP to Laravel to Javascript">
        <meta name="keywords" content="HTML, PHP, Laravel, VUE, Elixir, Eloquent, Envoyer, Forge, Javascript, Lumen, Servers, Spark, Testing, Forum, Web, Development, Programming, Coding, Front End, Back End, Server Coding, My Sql">

        
        <link rel="shortcut icon" type="image/x-icon" href="/img/codeqandalogo.ico">
        <link rel="stylesheet" href="/css/bootstrap.css" media="screen" title="no title">
        <link rel="stylesheet" href="/css/styles/template.css" media="screen" title="no title">
        <link href="https://fonts.googleapis.com/css?family=Catamaran|Courgette|PT+Mono" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato|Source+Sans+Pro" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
        @yield('stylesheets')
        

        <script src="http://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
        <script src="/js/main.js"></script>
        <script src="/js/app.js"></script>
        @yield('scriptsHeader')

    </head>
    <body>
        <div class="top-bar">
            <div class="ask-logo">
            </div>
            <div class="main-logo">
                <a href="/discuss">Code <img src="/img/codeqandalogo.png" class="codeqanda-main-logo">and A<span class="main-logo-com">.com</span></a>

            </div>
{{-- Show My Code Q and A if there is a user session --}}
            @if ( Auth::user() )
                <a href="/users/{{ '@' . Auth::user()->userName }}">
                    <div class="main-my-profile-header">
                        My Code QandA
                    </div>

                    
                    <div class="main-my-profile">
                        <img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( Auth::user()->email ) ) )}}?d={{ urlencode( "http://www.diaglobal.org/_Images/member/Generic_Image_Missing-Profile.jpg" )}}&s={{ 120 }}"
                                         class="main-profile-image">

                    </div>
                </a>
            @endif
        </div>
{{-- End my code q and a profile section --}}

        <div class="second-bar">

        </div>

            <div class="third-bar">

            </div>

    <div class="wrapper">

        <div class="second-bar-content">
            @include('layouts.navigation')
        </div>

        <div class="folder-navigation-div">
            <script src="/js/folder-navigation.js"></script>
        </div>

        <div class="content">
            @yield('content')
        </div>

    </div>

        

        @yield('scriptsBottom')

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-87693766-1', 'auto');
          ga('send', 'pageview');

        </script>
    </body>
</html>
