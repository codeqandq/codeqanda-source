<style>
    .modal {
        position: fixed;
        background-color: rgba(0, 0, 0, .4);
    }

    .modal-content {
        margin: 0 auto;
        top: 200px;
        color: #000;
        font-size: 2rem;
        border-radius: 0;
        background-color: #efefef;
        animation-name: slideIn;
        animation-duration: 0.4s;
        height: 55555;
        width: 600px;
    }

    .modal-content a {
        color: #fff;
    }

    .login-forgot-password {
        color: rgb(72, 103, 170);
    }

    .modal-form {
        padding: 5rem;
    }

    .login-heading {
        background-color: rgb(72, 103, 170);
        height: 8rem;
        color: #fff;
        padding: 2rem;
        font-size: 3rem;
        width: 100%;
    }

    .modal-content-text {
        height: 40px;
        font-size: 2rem;
        padding: 2rem;
        color: #000;
        width: 100%;
        transition: ease-in-out, width .35s ease-in-out;
    }

    .login-close {
        font-size: 2rem;
        float: right;
        font-color: #fff;
    }

    .modal-content-text:focus {
        font-size: 2rem;
        padding: 2rem;
    }

    /* Add Animation */
    @keyframes slideIn {
        from {top: 0; opacity: 1}
        to {top: 200px; opacity: 1}
    }

    @keyframes fadeIn {
        from {opacity: 1}
        to {opacity: 1}
    }
</style>


{{-- Login Form --}}
<div class="modal">
    <div class="modal-content">
        <div class="login-heading">
            Log in to CodeQandA <a class="login-close btn" id="login-close"><span>X</sapn></a>
        </div>
        <div class="modal-form">
        <!-- <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}"> -->
         <form class="form-horizontal" role="form" method="POST" id="login-form">
        {{ csrf_field() }}

        {{-- Email Field --}}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class=""
                   for="email">
            <span class="">Email</span>
            </label></br>
            <input class="modal-content-text"
                   type="email" id="email"
                   name="email"
                   required
                   value="{{ old('email') }}"/>
        </div>

        {{-- Password Field --}}
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class=""
                   for="password">
            <span class="">Password</span>
            </label></br>
            <input class="modal-content-text"
                   type="password" id="password"
                   name="password"
                   required
                   value="{{ old('password') }}"/>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Login
                </button>

                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                    <span class="login-forgot-password">Forgot Your Password?</span>
                </a>
            </div>
        </div>
        </div>
        </form>


    
    </div>
</div>



<script>

var form = $('#login-form');

form.submit(function(e) {
    $.ajax({
        url: '/login',
        method: 'post'
    });
});

</script>







