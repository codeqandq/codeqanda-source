@extends('layouts.template')

@section('stylesheets')
	<link rel="stylesheet" href="/css/userprofile.css">
	<link rel="stylesheet" href="/css/flags.css">
@stop

@section('scriptsHeader')
	<script src="/js/app.js"></script>
	<script src="/js/bootstrap-formhelpers.min.js"></script>
@stop

@section('content')


<div class="profile-wrapper">

	<img src="https://www.gravatar.com/avatar/{{ md5( strtolower( trim( $user->email ) ) )}}?d={{ urlencode( "http://www.diaglobal.org/_Images/member/Generic_Image_Missing-Profile.jpg" )}}&s={{ 200 }}"
								 class="profile-img">




	<div class="profile-info">

		<div class="profile-headings">
			<div class="profile-heading">
				Name: 
			</div>
			<div class="profile-heading">
				Username: 
			</div>
			<div class="profile-heading">
				Email: 
			</div>
			<div class="profile-heading">
				City: 
			</div>
			<div class="profile-heading">
				State: 
			</div>

		</div>
		<div class="profile-heading-info">
			<div class="profile-information">
				{{ $user->name }}
			</div>
			<div class="profile-information">
				{{ $user->userName }}
			</div>
			<div class="profile-information">
				{{ $user->email }}
			</div>
			<div class="profile-information">
				{{ $user->city }}
			</div>
			<div class="profile-information">
				{{ $user->state }}
			</div>
		</div>
	</div>

</div>

<div class="bottom-bar"><span class="bfh-countries profile-flag" data-country="{{ $user->country }}" data-flags="true"></span>Recent Activity</div>

<div class="profile-recent-activity">
	<div class="profile-recent-discussions">
		<div class="profile-recent-discussions-heading">Recent Discussions</div>
		<div class="profile-recent-discussions-body">
			@foreach ($discussions as $discussion)
				{{ $discussion->created_at->diffForHumans() }} - <a href="/discuss/channels/{{ $discussion->channel }}/{{ $discussion->id }}/{{ $discussion->slug }}">{{ $discussion->title }}</a><br><br>
			@endforeach
		</div>
	</div>

	<div class="profile-recent-comments">
		<div class="profile-recent-comments-heading">Recent Comments</div>
		<div class="profile-recent-comments-body">
			@foreach ($comments as $comment)
				{{ $comment->created_at->diffForHumans() }} commented on <a href="/discuss/channels/{{ $comment->discussion->channel }}/{{ $comment->discussion->id }}/{{ $comment->discussion->slug }}">{{ $comment->discussion->title }}</a><br><br>
			@endforeach
		</div>
	</div>
</div>

@stop