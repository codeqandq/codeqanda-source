<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

//User Routes
Route::get('/home', 'HomeController@index');
Route::post('/{user}/update', 'HomeController@update');


//Discussion Routes
Route::get('/discuss/create', 'DiscussionController@create');
Route::post('/discuss/create', 'DiscussionController@store');
Route::get('/discuss', 'DiscussionController@index');
Route::get('/discuss/channels/{channel}/{discussion}/{discussionSlug}', 'DiscussionController@show');
Route::post('/{discussion}/comment', 'DiscussionController@comment');
Route::get('/comment/like/{comment}', 'DiscussionController@like');
Route::get('/discuss/{channel}', 'DiscussionController@channel');
Route::get('/bestanswer/{discussion}/{comment}/{user}', 'DiscussionController@bestAnswer');

//User Profiles
Route::get('/users/@{userName}', 'DiscussionController@userProfile');

// Route::get('/test', function () {
// 	return view('auth.passwords.email');
// });
// 


